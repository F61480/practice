/* Aims to calculate various math problems */

#include <iostream>
#include <cmath>

using namespace std;

int main() {
    
    int a, b, c;
    cin >> a >> b >> c;
    
    // a). Simple addition, power of a number, multiplication
    int result1 = a + b - ((a*a) * (b*b*b) * (c*c*c*c));
    cout << result1 << endl;
    
    // b). Multiplication, addition, division
    int addition1 = (a * b) / c;
    int addition2 = c / (a * b);
    
    int result2 = addition1 + addition2;
    cout << result2 << endl;
    
    return 0;
}