/*             Calculates the following:
 * (1 + x/1! + x^2/2!) * (1 + x^3/3! + x^5/5!) */

#include <iostream>

using namespace std;

int main() {
    
    int x;
    cin >> x;
    
    int first_half = 1 + x/1 + (x * x) / (1 * 2);
    int second_half = 1 + ((x * x * x) / (1 * 2 * 3)) + ((x * x * x * x * x) / (1 * 2 * 3 * 4 * 5));
    
    int result = first_half * second_half;
    cout << result << endl;
    
    
    return 0;
}