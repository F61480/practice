/* Simple app to calculate medium sized factorials */

#include <iostream>

using namespace std;

int main() {
    
    int a;
    cin >> a;
    
    int b = 1;
    
    while(a > 0) {
        b *= a;
        a--;
    }
    cout << b << endl;
    
    return 0;
}