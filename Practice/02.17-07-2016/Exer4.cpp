#include <iostream>
#include <cmath>

using namespace std;

int main() {
    
    int result = sqrt(1 + sqrt(2 + sqrt(3 + sqrt(4))));
    cout << result << endl;
    
    return 0;
}