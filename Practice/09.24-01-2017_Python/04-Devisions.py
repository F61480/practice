# Read two integers and print two lines.
# The first line should contain integer division, // .
# The second line should contain float division, / .
# You don't need to perform any rounding or formatting operations.

if __name__ == '__main__':
    first = int(input())
    second = int(input())

    floorDev = first // second
    print(floorDev)

    dev = first / second
    print(dev)
