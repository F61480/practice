# Read an integer N. For all non-negative integers i < N, print i^2. See the sample for details.

if __name__ == '__main__':
    entry = int(input())
    for i in range(0, entry):
        print(pow(i, 2))
    i = 0
    while (i < entry):
        print(pow(i, 2))
        i += 1

