# Read an integer N.
# Without using any string methods, try to print the following:
# 123...N
# Note that "..." represents the values in between. 


if __name__ == '__main__':
    digit = int(input())

    for i in range(0, digit):
        print(i + 1, end="")

