# Read two integers from STDIN and print three lines where:
# The first line contains the sum of the two numbers.
# The second line contains the difference of the two numbers (first - second).
# The third line contains the product of the two numbers.

if __name__ == '__main__':
    first = int(input())
    second = int(input())

    sums = first + second
    print(sums)
    
    difference = first - second
    print(difference)

    addition = first * second
    print(addition)

