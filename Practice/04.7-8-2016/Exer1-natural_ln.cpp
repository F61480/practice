#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main() {

	double x, e;
	cin >> x >> e;

	double equation = pow(x, 4) + pow(e, x) + 10;
	double ln = log(equation);

	cout << "ln(x^4 + e^x + 10) = " << ln << endl;

	system("pause");

	return 0;
}