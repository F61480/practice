#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main() {

	double x;
	cin >> x;

	double equation = pow(x, 4) + pow(x, 2) + 8;
	double log = log10(equation);

	cout << "log(x^4 + x^2 + 8) = " << log << endl;

	system("pause");

	return 0;
}