#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main() {

	// p.77 - exer 4 a).
	int cossinus = cos(0) + abs(1 / (1 / 3 - 1));
	cout << "Cossinus: " << cossinus << endl;

	//b).
	int a;
	cin >> a;
	cout << "Result abs(a - 10) + sin(a - 1): " << abs(a - 10) + sin(a - 1) << endl;

	//c).
	double x;
	cin >> x;
	cout << cos(-2 + 2 * x) + sqrt(fabs(x - 5)) << endl;

	//d).
	double x1;
	cin >> x1;
	cout << sin(sin(x*x - 1) * sin(x * x - 1)) + cos(x * x * x - 1) * abs(x - 2) << endl;

	//e).
	double new_x, new_y, new_a;
	cin >> new_x >> new_y >> new_a;
	cout << sin(sin(new_x * new_x + 1)) + cos(new_x * new_x * new_x - 1) * cos(abs(new_x - 2) - 1) / new_y * new_a + sqrt(abs(new_y) - new_x) << endl;

	system("pause");

	return 0;
}