#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main() {

	// p.77 - exer 5 a).
	double a, b, c, d, e, f, h;
	cin >> a >> b >> c >> d >> e >> f >> h;
	cout << "a / (b * (c / (d * (e / (f * h))))) = " << a / (b * (c / (d * (e / (f * h))))) << endl;

	// b).
	double x, y;
	cin >> x >> y;
	cout << "(a + b) / (x - (2 * y)) = " << (a + b) / (x - (2 * y)) << endl;

	// c).
	cout << "a + ((b / (x - 2)) * y) = " << a + ((b / (x - 2)) * y) << endl;

	system("pause");

	return 0;
}