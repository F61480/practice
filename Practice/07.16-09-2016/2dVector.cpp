//Tihomir Mladenov 16-SEP-2016
//2d Vector

#include <iostream>
#include <vector>

using namespace std;


int main(){
    int row, column;
    cin >> row >> column;

    //Gets the size of a 2d vector;
    int vsize = 0;

    vector<vector<int> > matrix;

    for(int i = 0; i < row; i++) {
        matrix.push_back(vector<int>());

        for(int j = 0; j < column; j++) {
            int tmp;
            cin >> tmp;

            matrix[i].push_back(tmp);
        }
    }

    for(int k = 0; k < row; ++k) {
        for(int l = 0; l < column; ++l) {
            cout << matrix[k][l] << " ";
        }
        cout << endl;
    }

    for(int m = 0; m < matrix.size(); m++) {
        vsize += matrix[m].size();
    }

    cout << vsize << endl;

    //cout<<matrix.size()<<endl; // Remove the [0]
    //cout<<matrix[0].size()<<endl; // Remove one [0]

    return 0;
}
