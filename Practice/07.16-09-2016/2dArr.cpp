//Tihomir Mladenov 16-SEP-2016
//2d Array

#include <iostream>

using namespace std;


int main(){
    const int ROW = 3;
    const int COLUMN = 3;
    
    int matrix[ROW][COLUMN];
    
    for(int i = 0; i < ROW; i++) {
        for(int j = 0; j < COLUMN; j++) {
            cin >> matrix[i][j];
        }
    }
    
    for(int i = 0; i < ROW; i++) {
        for(int j = 0; j < COLUMN; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    
    return 0;
}
