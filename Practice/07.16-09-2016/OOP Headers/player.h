#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>

class Player{
private:
    std::string name;
    std::string date;
public:
    Player(std::string name, std::string date);
    std::string getName() const;
    std::string getDate() const;
};

#endif // PLAYER_H