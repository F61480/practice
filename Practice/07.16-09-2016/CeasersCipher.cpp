//Tihomir Mladenov 16-SEP-2016
//Ceaser's cipher

#include <iostream>

using namespace std;

string encode(string a, int& key) {
    string b = a;
    
    for(int i = 0; i < b.size(); i++) {
        int a1 = key;
        
        int char_ind = static_cast<int> (b[i]);
        int new_ind = char_ind + a1;
        
        b[i] = (char) new_ind;
    }
    
    return b;
}

void decode(int &dkey, string c) {
    for(int i = 0; i < c.size(); i++) {
        int d = dkey;
        
        int char_ind2 = static_cast<int> (c[i]);
        int new_ind2 = char_ind2 - d;
        
        c[i] = (char) new_ind2;
    }
    cout << "The decoded word is: " << c << endl;
}

int main(){
    cout << "Enter word to encode.\n";
    string word;
    cin >> word;
    
    cout << "Enter encode key.\n";
    int key;
    cin >> key;
    
    string encoding = encode(word, key);
    cout << "The encoded word is: " << encoding << endl;
    
    cout << "Would you like to decode the previously entered string - y/n?\n";
    string answer_decode;
    cin >> answer_decode;
    
    if(answer_decode == "y") {
        cout << "Enter the key to decode.\n";
        int decode_key;
        cin >> decode_key;
        
        decode(decode_key, encoding);
    }
    
    return 0;
}
