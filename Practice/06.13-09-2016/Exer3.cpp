//Tihomir Mladenov
//Code checks if digit 7 is contained without the entered number;

#include <iostream>

using namespace std;

const int a = 7;

int main() {
    int trippleDigit;
    cin >> trippleDigit;
    
    while (trippleDigit != 0) {
        int modulo = trippleDigit % 10; // Is equal to the last digit of trippleDigit;
        int devide = trippleDigit / 10; // Is equal to trippleDigit without the last digit, or is equal to 0 if trippleDigit < 10;
        
        if(modulo == a) {
            cout << "The digit contains " << a << endl;
            break;
        }
    }
    
    
    return 0;
}

