//Page 77; Exer. 6;

#include <iostream>
#include <cmath>

using namespace std;


int main() {
    //Perimeter of a square;
    int squarePerim;
    cin >> squarePerim;
    
    int PerimSqr = 4 * squarePerim;
    cout << "Perimeter is: " << PerimSqr << endl;
    
    //Are of a Equilateral triangle;
    int triangleSide;
    cin >> triangleSide;
    
    double triangleArea = (sqrt(3) / 4) * pow(triangleSide, 2);
    cout << "Area is: " << triangleArea << endl;
    
    return 0;
}

