//Page 78; Exer. 9;

#include <iostream>
#include <cmath>

using namespace std;


int main() {
    //IF digit is devided to 4 or 7, the boolean must be true.
    int p;
    cin >> p;
    
    bool isDeviding;
    
    if(p % 4 == 0 || p % 7 == 0) {
        isDeviding = true;
    }
    else {
        isDeviding = false;
    }
    
    if(isDeviding == 1) {
        cout << "The digit is devided.\n";
    }
    else {
        cout << "The digit is not devided.\n";
    }
    
    return 0;
}
