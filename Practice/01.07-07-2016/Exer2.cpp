/*Switches the position of two integers via multiplication.*/

#include <iostream>
#include <cstdio>

using namespace std;

int main() {

    printf("Enter two digits that you want to switch:\n");
    
    int a, b;
    scanf("%d", &a);
    scanf("%d", &b);
    
    int c = a;
    
    printf("Before swap, a = %d, b = %d \n", a, b);
    
    a *= b;
    a /= c;
    b = c;
    
    printf("After swap, a = %d, b = %d \n", a, b);
    
    return 0;
}
