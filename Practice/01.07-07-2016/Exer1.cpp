#include <iostream>

using namespace std;

int main() {

    cout << "a, b = ";
    int a, b;
    
    cin >> a >> b;
    cout << "The product of " << a << " and " << b << " is " << a*b << endl;
    
    return 0;
}