/* Transforms string to capital letters. */

#include <iostream>
#include <cstring>

using namespace std;

int main() {
    
    // Gets initial string;
    string data;
    getline(cin, data);
    
    // Gets initial string's lenght;
    char data_char[data.length()]; 
    
    // Converts string to char array;
    for(int i = 0; i < sizeof(data_char); i++) {
        data_char[i] = data[i];
    }
    
    // Cicles through char array, if ASCII index of
    // a char array element corresponds to low case,
    // ASCII index - 32 -> that element changes to capital.
    for(int j = 0; j < sizeof(data_char); j++) {
        
        int char_ind = static_cast<int> (data_char[j]); // Takes char's ASCII code position;
        
        if(char_ind >= 65 && char_ind <= 90) {
            data_char[j] = (char) char_ind;
        }
        else if(char_ind >= 97 && char_ind <= 122) {
            data_char[j] = (char) char_ind - 32;
        }
    }
    
    cout << data_char << endl;
    
    return 0;
}
