/*Tihomir Mladenov 16-DEC-2016
 * HackerRank:
 * https://www.hackerrank.com/domains/algorithms/warmup
 * "Diagonal Difference*/

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    
    //Define NxN matrix size;
    int n;
    cin >> n;
    
    vector< vector<int> > a(n,vector<int>(n));
    for(int a_i = 0;a_i < n;a_i++){
       for(int a_j = 0;a_j < n;a_j++){
          cin >> a[a_i][a_j];
       }
    }
    
    int row = 0; // used to cicle through the elements of each row;
    int sumPrimaryDiagonal = 0;
    int sumSecondaryDiagonal = 0;
    
    for(int i = 0; i < n; i++) {
        sumPrimaryDiagonal += a[i][row++];
    }
    
    //Zeroing row count for next step;
    row = 0;
    
    int i = 0;
    int j = 0;
    
    //Reversing the matrix;
    for(i = 0; i < n; i++) {
        
        for(j = 0; j < n / 2; j++) {
            int tmp = a[i][j];
            a[i][j] = a[i][n - j - 1];
            a[i][n - j - 1] = tmp;
        }
    }
    
    for(int i = 0; i < n; i++) {
        sumSecondaryDiagonal += a[i][row++];
    }
    
    //Print absolute value of Primary minus Secondary diagonals;
    cout << abs(sumPrimaryDiagonal - sumSecondaryDiagonal) << endl;
    
    return 0;
}

