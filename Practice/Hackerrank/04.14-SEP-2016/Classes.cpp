#include <iostream>
#include <sstream>
using namespace std;

class Student {
private:
    int age;
    int standard;
    string first_name;
    string last_name;
public:
    void set_age(int a1) {
        age = a1;
    }
    void set_standard(int b1) {
        standard = b1;
    }
    void set_first_name(string c1) {
        first_name = c1;
    }
    void set_last_name(string d1) {
        last_name = d1; 
    }
    int get_age() {
        return age;
    }
    string get_first_name() {
        return first_name;
    }
    string get_last_name() {
        return last_name;
    }
    int get_standard() {
        return standard;
    }
    string to_string() {
        stringstream b;
        b << age << "," << first_name << "," << last_name << "," << standard << endl;
        return b.str();
    }
};

int main() {
    int age, standard;
    string first_name, last_name;
    
    cin >> age >> first_name >> last_name >> standard;
    
    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    
    return 0;
}

