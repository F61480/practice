//Tihomir Mladenov 14-SEP-2016
//The class aims has to store:
//#Arr to store 5 scores;
//#Sum variable to store results;

class Student {
    private:
    int scores[5];
    int sum;
    public:
    void input() {
        for(int i = 0; i < 5; i++) {
            cin >> scores[i];
        }
    }
    int calculateTotalScore() {
        for(int i = 0; i < 5; i++) {
            sum += scores[i];
        }
        return sum;
    }
};
