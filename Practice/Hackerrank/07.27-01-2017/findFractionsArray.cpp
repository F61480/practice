/*

Given an array of integers, calculate which fraction of its elements are positive, which fraction of its elements are negative, and which fraction of its elements are zeroes, respectively. Print the decimal value of each fraction on a new line.

*/

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main()
{

    int n;
    cin >> n;
    
    double positive = 0;
    double negative = 0;
    double zeroes = 0;
    
    vector<int> myvec;
    
    for(int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        myvec.push_back(tmp);
    }
    
    for(int i = 0; i < myvec.size(); i++) {
        if(myvec[i] < 0) {
            negative++;
        }
        
        if(myvec[i] > 0) {
            positive++;
        }
        
        if(myvec[i] == 0) {
            zeroes++;
        }
    }
    
    cout << setprecision(6) << positive / n << endl;
    cout << setprecision(6) << negative / n << endl;
    cout << setprecision(6) << zeroes / n << endl;

    return 0;
}
