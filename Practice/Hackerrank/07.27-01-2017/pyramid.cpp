/*

Observe that its base and height are both equal to N, and the image is drawn using # symbols and spaces. The last line is not preceded by any spaces.

Write a program that prints a staircase of size N.

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <ios>

using namespace std;

int main()
{

    int n;
    cin >> n;

    string tmp;
    tmp.clear();

    for(int i = 0; i < n; i++) {
        tmp += "#";
        cout << setw(n) << setiosflags(ios::right) << tmp << endl;
    }

    return 0;
}
