/*

Circular Array rotation. Link to Hackerrank challenge:
https://www.hackerrank.com/challenges/circular-array-rotation

*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

void print(vector<int> tmp)
{
    for(int i = 0; i < tmp.size(); i++) {
        cout << tmp[i] << " ";
    }
    cout << endl;
}

int main()
{

    int n, k, q;
    cin >> n >> k >> q;

    vector<int> myvec(n);
    
    if(k > n) {
        k %= n;
    }
    
    for(int i = k; i < n; i++) {
        cin >> myvec[i];
    }
    
    for(int i = 0; i < k; i++) {
        cin >> myvec[i];
    }
    
    for(int i = 0; i < q; i++) {
        int m;
        cin >> m;
        cout << myvec[m] << endl;
    }

    return 0;
}
