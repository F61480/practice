/*

Hackerrank challange. Count how many times a number would fully devide itself by each of its digits.

*/


#include <iostream>
#include <vector>

using namespace std;

int main()
{

    int testCases;
    cin >> testCases;

    while(testCases--) {
        int entry;
        cin >> entry;

        int aCopy = entry;

        int tmp = 0;
        int counter = 0;

        while(entry) {
            tmp = entry % 10;
            entry /= 10;
            if((tmp != 0) && (aCopy % tmp) == 0) {
                counter++;
            }
        }

        cout << counter << endl;
    }

    return 0;
}
