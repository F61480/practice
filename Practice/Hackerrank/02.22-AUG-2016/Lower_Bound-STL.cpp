#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;


int main() {

	int N;
	cin >> N;

	vector<int>::iterator low;

	while (cin) {
		vector<int> vec;

		for (int i = 0; i < N; i++) {
			int digit;
			cin >> digit;
			vec.push_back(digit);
		}

		int Q;
		cin >> Q;

		while (Q > 0) {
			for (int j = 0; j < vec.size(); j++) {
				int guess;
				cin >> guess;
				if (guess == vec[j]) {
					cout << "Yes " << j + 1 << endl;
				}
				else if (guess != vec[j]){
					low = lower_bound(vec.begin(), vec.end(), N + 2);
					cout << "NO " << (low - vec.begin()) << endl;
				}
				Q--;
			}
		}
	}

	system("pause");

	return 0;
}