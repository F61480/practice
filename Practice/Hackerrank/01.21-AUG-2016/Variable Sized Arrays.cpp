#include <iostream>

using namespace std;


int main() {

	int N;
	int Q;

	cin >> N >> Q;

	int** ArrOfArr = new int*[N];


	for (int i = 0; i < N; i++) {
		int k;
		cin >> k;
		ArrOfArr[i] = new int[k];
		for (int j = 0; j < k; j++) {
			cin >> ArrOfArr[i][j];
		}
	}

	for (int i = 0; i < Q; i++) {
		int ind;
		int digit;
		cin >> ind >> digit;
		cout << ArrOfArr[ind][digit] << endl;
	}

	system("pause");

	return 0;
}