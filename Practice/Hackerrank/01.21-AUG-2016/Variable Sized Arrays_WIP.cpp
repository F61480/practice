#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

void vec_print(vector<int> &a) {
	for (int i = 0; i < a.size(); i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

void vec_null(vector<int> &b) {
	for (int i = 0; i < b.size(); i++) {
		b.pop_back();
	}
}

int main() {

	int N;
	int Q;

	int max = 0;

	cin >> N >> Q;

	vector<int> vec;

	while (Q > 0) {
		cout << "Enter Query element cout.\n";
		int elemCount;
		cin >> elemCount;

		cout << "Enter the elements of the query.\n";
		for (int i = 0; i < elemCount; i++) {
			int elemQuery;
			cin >> elemQuery;
			vec.push_back(elemQuery);
		}
		for (int i = 0; i < vec.size(); i++) {
			if (max < vec[i]) {
				max = vec[i];
			}
		}
		cout << max << endl;
		vec_null(vec);
		vec.pop_back();
		Q--;
	}

	system("pause");

	return 0;
}