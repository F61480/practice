/*Tihomir Mladenov 13-SEP-2016
# HackerRank string parsing
# challange.		    */

#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    stringstream b(str);
    char ch;
    int num = 0;
    
    vector<int> parse;
    
    for(int i = 0; i < str.size(); i++) {
        b >> num >> ch;
        parse.push_back(num);
    }
    return parse;
}

int main() {
    /*string a;
    cin >> a;
    
    stringstream b(a);
    
    char ch;
    int a1 = 0, b1 = 0, c = 0;
    
    b >> a1 >> ch >> b1 >> ch >> c;
    
    cout << a1 << " " << b1 << " " << c << endl;8*/
    //=========================================================
    /*string a;
    cin >> a;
    
    stringstream b(a);
    char ch;
    
    int parsedInts[a.size()];
    
    for(int i = 1; i < a.size() - 1; i++){
        b >> parsedInts[i] >> ch;
        cout << parsedInts[i] << " ";
    }*/
    
    string str;
    cin >> str;
    int numcheck = 0;
    
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        if(numcheck != integers[i]) {
            cout << integers[i] << "\n";
            numcheck = integers[i];
        }
        else
            break;
    }


    
    return 0;
}
