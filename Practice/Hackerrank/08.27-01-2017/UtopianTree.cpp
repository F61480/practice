/*

Link to Hackerrank challenge:
https://www.hackerrank.com/challenges/utopian-tree

*/



#include <iostream>
#include <string>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <stack>

using namespace std;

int main()
{
    int t;
    cin >> t;

    int h = 1;

    while(t--) {
        int i = 0;
        int n;
        cin >> n;
        
        bool spring = true;

        for(int i = 0; i < n; i++) {
            if(spring == true || spring == false) {
                if(spring == true) {
                    h *= 2;
                    spring = false;
                }
                else if (spring == false) {
                    h += 1;
                    spring = true;
                }
            }
        }
        cout << h << endl;
        h = 1;
    }

    return 0;
}
