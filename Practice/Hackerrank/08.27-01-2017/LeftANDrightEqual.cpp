/*

Link to Hackerrank challange:
https://www.hackerrank.com/challenges/sherlock-and-array

*/

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

void print(vector<int>& myvec)
{
    for(int i = 0; i < myvec.size(); i++) {
        cout << myvec[i] << " ";
    }
    cout << endl;
}

int main()
{
    int t;
    cin >> t;

    while(t--) {
        int n;
        cin >> n;

        vector<int> myvec(n);
        vector<int> mysecond(n);
        
        int sum = 0;
        bool found = false;
        
        cin >> myvec[0];
        mysecond[0] = myvec[0];

        for(int i = 1; i < n; i++) {
            cin >> myvec[i];
            mysecond[i] = myvec[i] + mysecond[i - 1];
        }
        
        sum = mysecond[n - 1];
        
        for(int i = 0; i < n - 1; i++) {
            if(sum == mysecond[i] + mysecond[i+1]) {
                found = true;
                break;
            }
        }
        if(found == true || n == 1) {
            cout << "YES" << endl;
        }
        else {
            cout << "NO" << endl;
        }
    }

    return 0;
}
