#include <iostream>
#include <string>
using namespace std;

int main() {
   string a, b;
    cin >> a >> b;
    int len_a = a.size();
    int len_b = b.size();
    
    cout << len_a << " " << len_b << endl;
    
    string c = a + b;
    cout << c << endl;
    
    char a0 = a[0];
    char b0 = b[0];
    
    a[0] = b0;
    b[0] = a0;
    
    cout << a << " "<< b << endl;
  
    return 0;
}
