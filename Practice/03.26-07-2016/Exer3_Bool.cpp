/* Write code where a bool takes value from a condition */

#include <iostream>
#include <cmath>

using namespace std;

int main() {
    
    int a1;
    cin >> a1;
    
    int b1;
    cin >> b1;
    
    int c1;
    cin >> c1;
    
    bool isntodd;
    bool belongs;
    bool notBelong;
    bool shared;
    bool negative;
    bool isEqual;
    
    if(a1 % 5 == 0) {
        isntodd = true;
    }
    else {
        isntodd = false;
    }
    
    cout << isntodd << endl;
    
    if(a1 >= 2 && a1 <= 6) {
        belongs = true;
    }
    else {
        belongs = false;
    }
    
    cout << belongs << endl;
    
    if (a1 >= 2 && a1 <= 6) {
        notBelong = false;
    }
    else {
        notBelong = true;
    }
    
    cout << notBelong << endl;
    
    if((a1 >= 2 && a1 <= 6) || (a1 >= -4 && a1 <= -2)) {
        shared = true;
    }
    else {
        shared = false;
    }
    
    cout << shared << endl;
    
    if(a1 < 0 || b1 < 0 || c1 < 0) {
        negative = true;
    }
    else {
        negative = false;
    }
    
    cout << negative << endl;
    
    if(a1 == b1 && b1 == c1) {
        isEqual = true;
    }
    else {
        isEqual = false;
    }
    
    cout << isEqual << endl;
    
    return 0;
}
