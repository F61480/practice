/* Write source to check whether the point belongs to a scheme X x Y*/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int x;
    cin >> x;
    
    int y;
    cin >> y;
    
    bool b1 = x*x + y*y <= 4 && y > 0;
    bool b2 = fabs(x) <=1 && y < 0 && y >= -2;
    
    cout << (b1 || b2) << endl;
    
    return 0;
}
