//24-SEP-2016 - Tihomir Mladenov
//Linear Algebra - Matrix Transpose
#include <iostream>

using namespace std;

void print(const int* matrix, const int &m, const int &n) {
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            cout << matrix[i*n+j] << " ";
        }
        cout << endl;
    }
}

int main()
{
    int m, n; cin >> m >> n;
    
    int matrix[m][n];
    
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            cin >> matrix[i][j];
        }
    }
    
    cout << "You have entered: " << endl;
    print((int*)matrix, m, n);
    
    int matrix_T[n][m];
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            matrix_T[i][j] = matrix[j][i];
        }
    }
    
    cout << "Transponated matrix is: " << endl;
    print((int*)matrix_T, n, m);
    
   return 0;
}
